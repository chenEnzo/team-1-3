"use strict";
let bookingID = retrieveLSData(BOOKING_INDEX_KEY);
let bookingData = consultSession.getBooking(bookingID)[0];
let taxiDataList = retrieveLSData(TAXI_DATA_KEY);
let listOfRoutes = [];
let index = 0;
if(bookingData) {
	let bookingDate = new Date(bookingData._bookingTime);
	let today = new Date();
	document.getElementById("bookingID").innerHTML = bookingData._bookingID;
	document.getElementById("pickupLocation").innerHTML = bookingData._pickupLocation;
	document.getElementById("destLocation").innerHTML = bookingData._destLocation;
	document.getElementById("taxiType").innerHTML = bookingData._taxiType;
	document.getElementById("taxiRego").innerHTML = bookingData._rego;
	document.getElementById("bookingTime").innerHTML = bookingDate.toDateString() + " " + bookingDate.toLocaleTimeString();
	document.getElementById("distance").innerHTML = bookingData._distance;
	document.getElementById("fare").innerHTML = bookingData._fare;
	if(bookingDate > today) {
		document.getElementById("taxiDetails").style.display = "block";
	} else {
		document.getElementById("taxiDetails").style.display = "none";
	}
	generateStopOverReadOnlyElement();
	let taxiDropdown = document.getElementById("ddTaxiType");
	let taxiListOptions = taxiDataList.map(taxi => taxi.type).filter((value, index, self) => self.indexOf(value) === index)
	let availbleTaxiList = taxiListOptions.filter(t => {
		return t != bookingData._taxiType;
	})
	for(let i = 0; i < availbleTaxiList.length; i++) {
		let taxiOpt = availbleTaxiList[i];
		let taxiEle = document.createElement("option");
		taxiEle.textContent = taxiOpt;
		taxiEle.value = taxiOpt;
		taxiDropdown.appendChild(taxiEle);
	}
	generateRequestForDirection(index);
}

function changeBooking() {
	let taxiType = document.getElementById("ddTaxiType").value;
	if(taxiType.length > 0) {
		let availbleTaxiList = taxiDataList.filter(t => {
			return t.type == taxiType && t.available === true
		})
		let newRego = "";
		if(availbleTaxiList.length > 0) {
			newRego = availbleTaxiList[0].rego;
		} else {
			alert("Sorry no " + taxiType + " available at this time. Plase try with different taxi type");
			return;
		}
		bookingData._taxiType = taxiType;
		bookingData._rego = newRego;
		let fare = "$" + fareCal(bookingData._distance, taxiType, bookingData._bookingTime).toFixed(2);
		bookingData._fare = fare;
		consultSession.updateBooking(bookingID, bookingData);
		updateLSData(APP_DATA_KEY, consultSession);
		// make old taxi availability true
		let oldTaxiIndex = taxiDataList.findIndex((t) => t.rego == bookingData._rego);
		taxiDataList[oldTaxiIndex].available = true;
		// make taxi availability false
		let taxiIndex = taxiDataList.findIndex((t) => t.rego == newRego);
		taxiDataList[taxiIndex].available = false;
		updateLSData(TAXI_DATA_KEY, taxiDataList);
		location.reload();
	}
}

function deleteBooking() {
	if(confirm("Are you sure you want to delete this booking?")) {
		consultSession.removeBooking(bookingID);
		updateLSData(APP_DATA_KEY, consultSession);
		// make taxi availability true
		let taxiIndex = taxiDataList.findIndex((t) => t.rego == bookingData._rego);
		taxiDataList[taxiIndex].available = true;
		updateLSData(TAXI_DATA_KEY, taxiDataList);
		window.location = "viewAllBookings.html";
	}
}

function generateRequestForDirection(index) {
	if(index < bookingData._listofTripCordinates.length) {
		let marker = new mapboxgl.Marker().setLngLat([bookingData._listofTripCordinates[index]._lngstart, bookingData._listofTripCordinates[index]._latstart]).addTo(map);
		if(index == bookingData._listofTripCordinates.length - 1) {
			let markerEnd = new mapboxgl.Marker().setLngLat([bookingData._listofTripCordinates[index]._lngend, bookingData._listofTripCordinates[index]._latend]).addTo(map);
		}
		sendXMLRequestForRoute(bookingData._listofTripCordinates[index]._latstart, bookingData._listofTripCordinates[index]._lngstart, bookingData._listofTripCordinates[index]._latend, bookingData._listofTripCordinates[index]._lngend, getDirection);
	}
}

function getDirection(dataObj) {
	const data = dataObj.routes[0];
	const route = data.geometry.coordinates;
	listOfRoutes = listOfRoutes.concat(route);
	drawPath();
	const bounds = new mapboxgl.LngLatBounds(listOfRoutes[0], listOfRoutes[0]);
	// Extend the 'LngLatBounds' to include every coordinate in the bounds result.
	for(const coord of listOfRoutes) {
		bounds.extend(coord);
	}
	map.fitBounds(bounds, {
		padding: 20
	});
	index = index + 1;
	generateRequestForDirection(index);
}

function drawPath() {
	if(listOfRoutes.length > 0) {
		const geojson = {
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'LineString',
				coordinates: listOfRoutes
			}
		};
		map.on('load', function() {
			// if the route already exists on the map, we'll reset it using setData
			if(map.getSource('route')) {
				map.getSource('route').setData(geojson);
			} else {
				map.addLayer({
					id: 'route',
					type: 'line',
					source: {
						type: 'geojson',
						data: geojson
					},
					layout: {
						'line-join': 'round',
						'line-cap': 'round'
					},
					paint: {
						'line-color': '#3887be',
						'line-width': 5,
						'line-opacity': 0.75
					}
				});
			}
		});
	}
}

function generateStopOverReadOnlyElement() {
	if(bookingData._stopOver.length > 0) {
		let ul = document.createElement('ul');
		ul.setAttribute("class", "mdl-list");
		bookingData._stopOver.forEach(function(stop) {
			let li = document.createElement('li');
			li.setAttribute("class", "mdl-list__item mdl-list__item--three-line");
			let span1 = document.createElement('span');
			span1.setAttribute("class", "mdl-list__item-primary-content");
			let eleI = document.createElement('i');
			eleI.setAttribute("class", "material-icons mdl-list__item-avatar");
			eleI.innerHTML = "place"
			let childSpan1 = document.createElement('span');
			childSpan1.innerHTML = stop.type + " - " + stop.location
			span1.appendChild(eleI);
			span1.appendChild(childSpan1);
			li.appendChild(span1)
			ul.appendChild(li);
		});
		document.getElementById("stopOvers").appendChild(ul);
	}
}