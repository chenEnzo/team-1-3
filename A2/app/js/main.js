"use strict";
let ListOfLocations = [];
let ListOfStopOvers = [];
let listOfRoutes = [];
let listOfMarker = [];
let listOfPopup = [];
let ListOfTripRouteCordinates = [];
let taxiDataList = retrieveLSData(TAXI_DATA_KEY);

document.getElementById("meeting-time").min = new Date().toISOString().split(".")[0];
document.getElementById("meeting-time").value = new Date().toISOString().split(".")[0];

let popupPickup = new mapboxgl.Popup({
	offset: 30
});
let markerPickup = new mapboxgl.Marker({
	draggable: true,
	color: "#b40219"
}).setLngLat(caulfield).addTo(map);
getUserCurrentLocationUsingGeolocation(function setCurrentLocation(lat, lng) {
	let currentLocation = [lng, lat];
	sendWebServiceRequestForReverseGeocoding(lat, lng, 'setPickupField');
	setPickupLocationonMap(lng, lat, "");
});
markerPickup.on('dragend', onPickupDragEnd);

function onPickupDragEnd() {
	const lngLat = markerPickup.getLngLat();
	sendWebServiceRequestForReverseGeocoding(lngLat.lat, lngLat.lng, 'setPickupField');
}

function setPickupField(data) {
	let lat = data.results[0].geometry.lat;
	let lng = data.results[0].geometry.lng;
	let location = data.results[0].formatted;
	popupPickup.setLngLat([lng, lat]).setHTML(location).addTo(map);
	document.getElementById("pickupLocation").value = location;
	let tripLoc = new TripLocation("start", lng, lat, location);
	addUpdateTripLocation("start", tripLoc)
}

document.getElementById("pickupLocation").addEventListener('blur', (event) => {
	if(event.target.value.length > 0) {
		sendWebServiceRequestForForwardGeocoding(event.target.value, 'setLocationOnMap')
	} else {
		markerPickup.remove();
		popupPickup.remove();
		let index = ListOfLocations.findIndex((e) => e.type === "start");
		if(index !== -1) {
			ListOfLocations.splice(index, 1);
		}
	}
});

function setLocationOnMap(data) {
	if(data && data.results.length > 0) {
		setPickupLocationonMap(data.results[0].geometry.lng, data.results[0].geometry.lat, data.results[0].formatted);
	} else {
		alert("address not fount. please enter proper address");
		document.getElementById("pickupLocation").value = "";
	}
}

function setPickupLocationonMap(lng, lat, location) {
	if(location.length > 0) {
		popupPickup.setLngLat([lng, lat]).setHTML(location).addTo(map);
	}
	markerPickup.setLngLat([lng, lat]).addTo(map);
	map.setCenter([lng, lat]);
	let tripLoc = new TripLocation("start", lng, lat, location);
	addUpdateTripLocation("start", tripLoc);
}
let markerDest = new mapboxgl.Marker({
	draggable: true,
	color: "#b40219"
});
let popupDest = new mapboxgl.Popup({
	offset: 30
});
markerDest.on('dragend', onDestiDragEnd);

function onDestiDragEnd() {
	const lngLat = markerDest.getLngLat();
	sendWebServiceRequestForReverseGeocoding(lngLat.lat, lngLat.lng, 'setDestField');
}

function setDestField(data) {
	let lat = data.results[0].geometry.lat;
	let lng = data.results[0].geometry.lng;
	let location = data.results[0].formatted;
	popupDest.setLngLat([lng, lat]).setHTML(location).addTo(map);
	document.getElementById("destLocation").value = location;
	let tripLoc = new TripLocation("end", lng, lat, location);
	addUpdateTripLocation("end", tripLoc)
}
document.getElementById("destLocation").addEventListener('blur', (event) => {
	if(event.target.value.length > 0) {
		sendWebServiceRequestForForwardGeocoding(event.target.value, 'getDestinationCord')
	} else {
		let index = ListOfLocations.findIndex((e) => e.type === "end");
		if(index !== -1) {
			markerDest.remove();
			ListOfLocations.splice(index, 1);
			displayDirection();
		}
	}
});

function getDestinationCord(data) {
	if(data && data.results.length > 0) {
		let lat = data.results[0].geometry.lat;
		let lng = data.results[0].geometry.lng;
		let location = data.results[0].formatted;
		popupDest.setLngLat([lng, lat]).setHTML(location).addTo(map);
		markerDest.setLngLat([lng, lat]).addTo(map);
		let tripLoc = new TripLocation("end", lng, lat, location);
		addUpdateTripLocation("end", tripLoc);
	} else {
		alert("address not fount. please enter proper address");
		document.getElementById("destLocation").value = "";
	}
}
document.getElementById("stopOvers").addEventListener('blur', (event) => {
	sendWebServiceRequestForForwardGeocoding(event.target.value, 'processStopOver');
});

function processStopOver(data) {
	console.log(data);
	if(data && data.results.length > 0) {
		console.log(data.results[0].formatted);
		let lat = data.results[0].geometry.lat;
		let lng = data.results[0].geometry.lng;
		let location = data.results[0].formatted;
		if(ListOfStopOvers.length > 0) {
			let typeOflastStop = "Stop Over " + (ListOfStopOvers.length + 1);
			ListOfStopOvers.push(new TripLocation(typeOflastStop, lng, lat, location))
		} else {
			ListOfStopOvers.push(new TripLocation("Stop Over 1", lng, lat, location))
		}
		displayDirection();
		generateStopOverElement();
		document.getElementById("stopOvers").value = "";
	} else {
		alert("address not fount. please enter proper address")
	}
}

function addUpdateTripLocation(key, tripLoc) {
	let index = ListOfLocations.findIndex((e) => e.type === key);
	if(index === -1) {
		ListOfLocations.push(tripLoc);
	} else {
		ListOfLocations[index] = tripLoc;
	}
	console.log(ListOfLocations);
	displayDirection();
}
let index = 0;

function displayDirection() {
	listOfRoutes = [];
	ListOfTripRouteCordinates = []
	clearMarkersPopup();
	let startLoc = ListOfLocations.filter(loc => {
		return loc.type === "start"
	})
	let endLoc = ListOfLocations.filter(loc => {
		return loc.type === "end"
	})
	if(startLoc.length > 0 && (ListOfStopOvers.length > 0 || endLoc.length > 0)) {
		if(ListOfStopOvers.length > 0) {
			for(let i = 0; i < ListOfStopOvers.length; i++) {
				let popup = new mapboxgl.Popup({
					offset: 30
				}).setLngLat([ListOfStopOvers[i]._lng, ListOfStopOvers[i]._lat]).setHTML(ListOfStopOvers[i].location).addTo(map);
				listOfPopup.push(popup);
				let marker = new mapboxgl.Marker().setLngLat([ListOfStopOvers[i]._lng, ListOfStopOvers[i]._lat]).addTo(map);
				listOfMarker.push(marker);
				if(i == 0) {
					//sendXMLRequestForRoute(startLoc[0]._lat, startLoc[0]._lng, ListOfStopOvers[i]._lat, ListOfStopOvers[i]._lng, getDirection);
					ListOfTripRouteCordinates.push(new TripRouteCordinates(startLoc[0]._lng, startLoc[0]._lat, ListOfStopOvers[i]._lng, ListOfStopOvers[i]._lat));
				} else {
					//sendXMLRequestForRoute(ListOfStopOvers[i - 1]._lat, ListOfStopOvers[i - 1]._lng, ListOfStopOvers[i]._lat, ListOfStopOvers[i]._lng, getDirection);
					ListOfTripRouteCordinates.push(new TripRouteCordinates(ListOfStopOvers[i - 1]._lng, ListOfStopOvers[i - 1]._lat, ListOfStopOvers[i]._lng, ListOfStopOvers[i]._lat));
				}
			}
			if(endLoc.length > 0) {
				//sendXMLRequestForRoute(ListOfStopOvers[ListOfStopOvers.length - 1]._lat, ListOfStopOvers[ListOfStopOvers.length - 1]._lng, endLoc[0]._lat, endLoc[0]._lng, getDirection);
				ListOfTripRouteCordinates.push(new TripRouteCordinates(ListOfStopOvers[ListOfStopOvers.length - 1]._lng, ListOfStopOvers[ListOfStopOvers.length - 1]._lat, endLoc[0]._lng, endLoc[0]._lat));
			}
		} else {
			//sendXMLRequestForRoute(startLoc[0]._lat, startLoc[0]._lng, endLoc[0]._lat, endLoc[0]._lng, getDirection);
			ListOfTripRouteCordinates.push(new TripRouteCordinates(startLoc[0]._lng, startLoc[0]._lat, endLoc[0]._lng, endLoc[0]._lat));
			// let bounds = [
			// 	[startLoc[0]._lng, startLoc[0]._lat],
			// 	[endLoc[0]._lng, endLoc[0]._lat]
			// ];
			// map.fitBounds(bounds);
		}
	}
	index = 0;
	generateRequestForDirection(index);
}

function generateRequestForDirection(index) {
	if(index < ListOfTripRouteCordinates.length) {
		console.log(ListOfTripRouteCordinates[index]._latstart);
		//getDirection1("tets");
		sendXMLRequestForRoute(ListOfTripRouteCordinates[index]._latstart, ListOfTripRouteCordinates[index]._lngstart, ListOfTripRouteCordinates[index]._latend, ListOfTripRouteCordinates[index]._lngend, getDirection);
	}
}

function getDirection(dataObj) {
	const data = dataObj.routes[0];
	const route = data.geometry.coordinates;
	listOfRoutes = listOfRoutes.concat(route);
	drawPath();
	const bounds = new mapboxgl.LngLatBounds(listOfRoutes[0], listOfRoutes[0]);
	// Extend the 'LngLatBounds' to include every coordinate in the bounds result.
	for(const coord of listOfRoutes) {
		bounds.extend(coord);
	}
	map.fitBounds(bounds, {
		padding: 20
	});
	index = index + 1;
	generateRequestForDirection(index);
}

function clearMarkersPopup() {
	listOfMarker.forEach((marker) => marker.remove());
	listOfMarker = [];
	listOfPopup.forEach((popup) => popup.remove());
	listOfPopup = [];
}

function drawPath() {
	if(listOfRoutes.length > 0) {
		const geojson = {
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'LineString',
				coordinates: listOfRoutes
			}
		};
		// if the route already exists on the map, we'll reset it using setData
		if(map.getSource('route')) {
			map.getSource('route').setData(geojson);
		} else {
			map.addLayer({
				id: 'route',
				type: 'line',
				source: {
					type: 'geojson',
					data: geojson
				},
				layout: {
					'line-join': 'round',
					'line-cap': 'round'
				},
				paint: {
					'line-color': '#3887be',
					'line-width': 5,
					'line-opacity': 0.75
				}
			});
		}
	}
}

function deleteStop(type) {
	ListOfStopOvers = ListOfStopOvers.filter(function(stop) {
		return stop.type !== type;
	});
	ListOfStopOvers.forEach(function(stop, index) {
		stop._type = "Stop Over " + (index + 1)
	});
	displayDirection();
	generateStopOverElement();
}

function generateStopOverElement() {
	document.getElementById("listOfStopOver").innerHTML = "";
	if(ListOfStopOvers.length > 0) {
		let ul = document.createElement('ul');
		ul.setAttribute("class", "mdl-list");
		ListOfStopOvers.forEach(function(stop) {
			let li = document.createElement('li');
			li.setAttribute("class", "mdl-list__item mdl-list__item--three-line");
            
			let span1 = document.createElement('span');
			span1.setAttribute("class", "mdl-list__item-primary-content");
			let eleI = document.createElement('i');
			eleI.setAttribute("class", "material-icons mdl-list__item-avatar");
			eleI.innerHTML = "place"
			let childSpan1 = document.createElement('span');
			childSpan1.innerHTML = stop.type + " - " + stop.location
			span1.appendChild(eleI);
			span1.appendChild(childSpan1);
			li.appendChild(span1)
            
			let span2 = document.createElement('span');
			span2.setAttribute("class", "mdl-list__item-secondary-content");
			let eleA1 = document.createElement('a');
			eleA1.setAttribute("class", "mdl-list__item-secondary-action");
			eleA1.setAttribute("onclick", "deleteStop('" + stop.type + "')");
			let eleI2 = document.createElement('i');
			eleI2.setAttribute("class", "material-icons");
			eleI2.innerHTML = "delete"
			eleA1.appendChild(eleI2);
			span2.appendChild(eleA1);
			li.appendChild(span2)
			ul.appendChild(li);
		});
		document.getElementById("listOfStopOver").appendChild(ul);
	}
}

let taxiDropdown = document.getElementById("taxiType");
let taxiListOptions = taxiDataList.map(taxi => taxi.type).filter((value, index, self) => self.indexOf(value) === index)
for(let i = 0; i < taxiListOptions.length; i++) {
	let taxiOpt = taxiListOptions[i];
	let taxiEle = document.createElement("option");
	taxiEle.textContent = taxiOpt;
	taxiEle.value = taxiOpt;
	taxiDropdown.appendChild(taxiEle);
}

function calculateFareDistance() {
	let distance = 0
	let taxiType = document.getElementById("taxiType").value;
	let bookingTime = document.getElementById("meeting-time").value;
	ListOfTripRouteCordinates.forEach(function(r) {
		distance += distanceCal(r.latstart, r.latend, r.lngstart, r.lngend);
	});
	if(distance > 0) {
		document.getElementById("distance").innerHTML = distance.toFixed(2) + " km";
		document.getElementById("fare").innerHTML = "$" + fareCal(distance, taxiType, bookingTime).toFixed(2);
	}
}

document.getElementById("reviewDiv").style.display = "none";

function reviewBooking() {
	let bookingTime = document.getElementById("meeting-time").value;
	let pickupLocation = document.getElementById("meeting-time").value;
	let destLocation = document.getElementById("destLocation").value;
	let taxiType = document.getElementById("taxiType").value;
	if(checkFormData(pickupLocation, destLocation, taxiType) == true) {
		return;
	}
	document.getElementById("bookingDiv").style.display = "none";
	document.getElementById("reviewDiv").style.display = "block";
	let calDistance = 0
	ListOfTripRouteCordinates.forEach(function(r) {
		calDistance += distanceCal(r.latstart, r.latend, r.lngstart, r.lngend);
	});
	let distance = calDistance.toFixed(2) + " km";
	let fare = "$" + fareCal(calDistance, taxiType, bookingTime).toFixed(2);
	let bookingDate = new Date(bookingTime);
	document.getElementById("roPickupLocation").innerHTML = pickupLocation;
	document.getElementById("roDestLocation").innerHTML = destLocation;
	document.getElementById("roTaxiType").innerHTML = taxiType;
	document.getElementById("roBookingtime").innerHTML = bookingDate.toDateString() + " " + bookingDate.toLocaleTimeString();
	document.getElementById("roDistance").innerHTML = distance;
	document.getElementById("roFare").innerHTML = fare;
	generateStopOverReadOnlyElement();
}

function generateStopOverReadOnlyElement() {
	document.getElementById("roListOfStopOver").innerHTML = "";
	if(ListOfStopOvers.length > 0) {
		let ul = document.createElement('ul');
		ul.setAttribute("class", "mdl-list");
		ListOfStopOvers.forEach(function(stop) {
			let li = document.createElement('li');
			li.setAttribute("class", "mdl-list__item mdl-list__item--three-line");
			let span1 = document.createElement('span');
			span1.setAttribute("class", "mdl-list__item-primary-content");
			let eleI = document.createElement('i');
			eleI.setAttribute("class", "material-icons mdl-list__item-avatar");
			eleI.innerHTML = "place"
			let childSpan1 = document.createElement('span');
			childSpan1.innerHTML = stop.type + " - " + stop.location
			span1.appendChild(eleI);
			span1.appendChild(childSpan1);
			li.appendChild(span1)
			ul.appendChild(li);
		});
		document.getElementById("roListOfStopOver").appendChild(ul);
	}
}

function changeBooking() {
	document.getElementById("bookingDiv").style.display = "block";
	document.getElementById("reviewDiv").style.display = "none";
	Array.from(document.getElementsByClassName("reviewEle")).forEach(function(element) {
		element.innerHTML = "";
	});
}

function confirmBooking() {
	let taxiType = document.getElementById("taxiType").value;
	let bookingTime = document.getElementById("meeting-time").value;
	let pickupLocation = document.getElementById("pickupLocation").value;
	let destLocation = document.getElementById("destLocation").value;
	if(checkFormData(pickupLocation, destLocation, taxiType) == true) {
		return;
	}
    
	let availbleTaxiList = taxiDataList.filter(t => {
		return t.type == taxiType && t.available === true
	})
	let rego = "";
	if(availbleTaxiList.length > 0) {
		rego = availbleTaxiList[0].rego;
	} else {
		alert("Sorry no " + taxiType + " available at this time. Plase try with different taxi type");
		return;
	}
	let calDistance = 0
	ListOfTripRouteCordinates.forEach(function(r) {
		calDistance += distanceCal(r.latstart, r.latend, r.lngstart, r.lngend);
	});
	let distanceObj = calDistance.toFixed(2);
	let distance = distanceObj + " km";
	let fare = "$" + fareCal(calDistance, taxiType, bookingTime).toFixed(2);
	let lenOfBookingList = consultSession.bookingList.length;
	let bookingId = 1;
	if(lenOfBookingList > 0) {
		bookingId = consultSession.getBookingByIndex(lenOfBookingList - 1).bookingID + 1;
	}
    
	let bookingData = new BookingData(bookingId, bookingTime, pickupLocation, destLocation, taxiType, rego, fare, distanceObj);
	ListOfStopOvers.forEach(function(stop) {
		bookingData.addStopOver(stop.type, stop.lng, stop.lat, stop.location);
	});
    
	ListOfTripRouteCordinates.forEach(function(t) {
		bookingData.addCordinates(t._lngstart, t._latstart, t._lngend, t._latend);
	});
    
	consultSession.bookingList.push(bookingData);
	console.log(consultSession);
	updateLSData(APP_DATA_KEY, consultSession);
	alert("Booking is confirmed");
    
	// make taxi availability false
	let taxiIndex = taxiDataList.findIndex((t) => t.rego == rego);
	taxiDataList[taxiIndex].available = false;
	updateLSData(TAXI_DATA_KEY, taxiDataList);
	updateLSData(BOOKING_INDEX_KEY, bookingId);
	window.location = "viewABooking.html";
}

function checkFormData(pickupLocation, destLocation, taxiType) {
	let pickupLocationError = document.getElementById("pickupLocation_msg");
	let destLocationError = document.getElementById("destLocation_msg");
	let taxiTypeError = document.getElementById("taxiType_msg");
	let errorExist = false;
	if(pickupLocation.length === 0) {
		pickupLocationError.innerHTML = "Please enter Pick up Location";
		errorExist = true;
	}
	if(destLocation.length === 0) {
		destLocationError.innerHTML = "Please enter Destination";
		errorExist = true;
	}
	if(taxiType.length === 0) {
		taxiTypeError.innerHTML = "Please select taxi type";
		errorExist = true;
	}
	return errorExist;
}