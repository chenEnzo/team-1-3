"use strict";

function generateBookingList(bookingData) {
	document.getElementById("futureBooking").innerHTML = "";
	document.getElementById("pastBooking").innerHTML = "";
    
	let ulFuture = document.createElement('ul');
	ulFuture.setAttribute("class", "demo-list-item mdl-list");
	let ulPast = document.createElement('ul');
	ulPast.setAttribute("class", "demo-list-item mdl-list");
    
	for(let i = 0; i < bookingData.length; i++) {
		let bookingDate = new Date(bookingData[i]._bookingTime);
		let today = new Date()
		let li = document.createElement('li');
		li.setAttribute("class", "mdl-list__item");
        
		let span1 = document.createElement('span');
		span1.setAttribute("class", "mdl-list__item-primary-content");
		let eleI = document.createElement('i');
		eleI.setAttribute("class", "material-icons mdl-list__item-avatar");
		eleI.innerHTML = "drive_eta"
		span1.appendChild(eleI);
		let childSpan1 = document.createElement('span');
		childSpan1.innerHTML = bookingData[i]._bookingID;
		span1.appendChild(childSpan1);
		li.appendChild(span1)
        
		let span4 = document.createElement('span');
		span4.setAttribute("class", "mdl-list__item-primary-content");
		let childSpan2 = document.createElement('span');
		childSpan2.innerHTML = bookingDate.toDateString() + " " + bookingDate.toLocaleTimeString();
		span4.appendChild(childSpan2);
		li.appendChild(span4)
        
		let span2 = document.createElement('span');
		span2.setAttribute("class", "mdl-list__item-primary-content");
		let eleA = document.createElement('a');
		eleA.setAttribute("class", "mdl-list__item-secondary-action");
		eleA.setAttribute("onclick", "view(" + bookingData[i]._bookingID + ")");
		let eleI1 = document.createElement('i');
		eleI1.setAttribute("class", "material-icons");
		eleI1.innerHTML = "info"
		eleA.appendChild(eleI1);
		span2.appendChild(eleA);
		li.appendChild(span2)
        
		if(bookingDate > today) {
			ulFuture.appendChild(li);
		} else {
			ulPast.appendChild(li);
		}
	}
    
	if(ulFuture.childElementCount > 0) {
		document.getElementById("futureBooking").appendChild(ulFuture);
	} else {
		document.getElementById("futureBooking").innerHTML = "There is no future booking";
	}
	if(ulPast.childElementCount > 0) {
		document.getElementById("pastBooking").appendChild(ulPast);
	} else {
		document.getElementById("pastBooking").innerHTML = "There is no past booking";
	}
}
generateBookingList(consultSession._bookingList);

function view(bookingId) {
	updateLSData(BOOKING_INDEX_KEY, bookingId);
	window.location = "viewABooking.html";
}