"use strict";

class TripLocation {
	constructor(type, lng, lat, location) {
		this._type = type;
		this._lng = lng;
		this._lat = lat;
		this._location = location;
	}
	get type() {
		return this._type;
	}
	get lng() {
		return this._lng;
	}
	get lat() {
		return this._lat;
	}
	get location() {
		return this._location;
	}
}

class TripRouteCordinates {
	constructor(lngstart, latstart, lngend, latend) {
		this._lngstart = lngstart;
		this._latstart = latstart;
		this._lngend = lngend;
		this._latend = latend;
	}
	get lngstart() {
		return this._lngstart;
	}
	get latstart() {
		return this._latstart;
	}
	get lngend() {
		return this._lngend;
	}
	get latend() {
		return this._latend;
	}
}

class BookingData {
	constructor(bookingID, bookingTime, pickupLocation, destLocation, taxiType, rego, fare, distance) {
		this._bookingID = bookingID;
		this._bookingTime = bookingTime;
		this._pickupLocation = pickupLocation;
		this._stopOver = [];
		this._destLocation = destLocation;
		this._taxiType = taxiType;
		this._rego = rego;
		this._fare = fare;
		this._distance = distance;
		this._listofTripCordinates = [];
	}
	get bookingID() {
		return this._bookingID;
	}
	get bookingTime() {
		return this._bookingTime;
	}
	get pickupLocation() {
		return this._pickupLocation;
	}
	get destLocation() {
		return this._destLocation;
	}
	get taxiType() {
		return this._taxiType;
	}
	get rego() {
		return this._rego;
	}
	get fare() {
		return this._fare;
	}
	get distance() {
		return this._distance;
	}
	addStopOver(type, lng, lat, location) {
		this._stopOver.push(new TripLocation(type, lng, lat, location));
	}
	addCordinates(lngstart, latstart, lngend, latend) {
		this._listofTripCordinates.push(new TripRouteCordinates(lngstart, latstart, lngend, latend));
	}
}

class Session {
	constructor() {
		this._bookingList = [];
	}
	get bookingList() {
			return this._bookingList;
		}
		// addBooking(bookingTime, pickupLocation, destLocation, taxiType, rego, fare, distance) {
		//     this._bookingList.push(new BookingData(bookingTime, pickupLocation, destLocation, taxiType, rego, fare, distance));
		// }
	removeBooking(bookingID) {
		let bookingIndex = this._bookingList.findIndex((b) => b.bookingID === bookingID);
		this._bookingList.splice(bookingIndex, 1);
	}
	getBookingByIndex(bookingIndex) {
		return this._bookingList[bookingIndex];
	}
	getBooking(bookingID) {
		//return this._bookingList[bookingIndex];
		return this._bookingList.filter(b => {
			return b.bookingID === bookingID
		})
	}
	updateBooking(bookingID, bookingData) {
		let index = this._bookingList.findIndex((b) => b.bookingID === bookingID);
		if(index !== -1) {
			this._bookingList[index] = bookingData;
		}
	}
	fromData(sessionData) {
		if(sessionData) {
			this._bookingList = [];
			let result = [];
			sessionData._bookingList.forEach(function(b) {
				let bookinObj = new BookingData(b._bookingID, b._bookingTime, b._pickupLocation, b._destLocation, b._taxiType, b._rego, b._fare, b._distance);
				b._stopOver.forEach(function(s) {
					bookinObj.addStopOver(s._type, s._lng, s._lat, s._location);
				});
				b._listofTripCordinates.forEach(function(t) {
					bookinObj.addCordinates(t._lngstart, t._latstart, t._lngend, t._latend);
				});
				result.push(bookinObj);
			});
			this._bookingList = result;
		}
	}
}

function distanceCal(lat1, lat2, lon1, lon2) {
	const R = 6371e3; // metres
	const dlat1 = lat1 * Math.PI / 180; // φ, λ in radians
	const dlat2 = lat2 * Math.PI / 180;
	const dlat = (lat2 - lat1) * Math.PI / 180;
	const dlon = (lon2 - lon1) * Math.PI / 180;
	const a = Math.sin(dlat / 2) * Math.sin(dlat / 2) + Math.cos(dlat1) * Math.cos(dlat2) * Math.sin(dlon / 2) * Math.sin(dlon / 2);
	const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	const d = R * c; // in metres
	return d / 1000;
}

function fareCal(d, taxiType, bookingTime) {
	let vechicleRate = 0;
	let flateRate = 4.2;
	let disRate = 1.622 * d;
	if(taxiType == "SUV") {
		vechicleRate = 3.5;
	} else if(taxiType == "Van") {
		vechicleRate = 6;
	} else if(taxiType == "Minibus") {
		vechicleRate = 10;
	}
	let bookingD = new Date(bookingTime);
	let startTime = new Date(bookingTime);
	startTime.setHours(17, 0, 0);
	let endTime = new Date(bookingTime);
	endTime.setDate(endTime.getDate() + 1);
	endTime.setHours(9, 0, 0);
	let nightLevvy = 0;
	if(bookingD >= startTime && bookingD <= endTime) {
		nightLevvy = 0.2 * (flateRate + vechicleRate + disRate);
	}
	let fare = flateRate + vechicleRate + disRate + nightLevvy;
	return fare;
}

let consultSession = new Session();
if(checkLSData(APP_DATA_KEY)) {
	// if LS data does exist
	let data = retrieveLSData(APP_DATA_KEY);
	consultSession.fromData(data);
}
if(checkLSData(TAXI_DATA_KEY) == false) {
	// if LS data doesn’t exist  
	updateLSData(TAXI_DATA_KEY, taxiList);
}