function jumpSearch(array, target) {

	let Arraylength = array.length
	let step = Math.floor(Math.sqrt(Arraylength));
	let blockStart = 0, currentStep = step;

	while (array[Math.min(currentStep, Arraylength) - 1] < target) {
		blockStart = currentStep;
		currentStep += step;

		if (blockStart >= Arraylength)
			return -1;
	}

	while (array[blockStart] < target)
	{
		blockStart++;
		if (blockStart == Math.min(currentStep, Arraylength))
			return -1;
	}

	if (array[blockStart] == target)
		return blockStart
	else
		return -1;
    
}
//test
//var arr=[1,2,3,4,5,6]
//jumpSearch(arr,3)