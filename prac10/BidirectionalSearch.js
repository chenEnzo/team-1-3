/* Write your function for implementing the Bi-directional Search algorithm here */

function bidirectionalSearchTest(array){ 
    let mid = array.length/2; 
     
    for(i = 0; i < mid; i++){ 
        if(array[mid-i].emergency === true){ 
            return array[mid-i].address; 
        } 
        if(array[mid+i].emergency === true){ 
            return array[mid+i].address; 
        } 
    } 
   return null; 
}